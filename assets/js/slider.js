/* From https://www.w3schools.com/howto/tryit.asp?filename=tryhow_js_image_compare */

function initComparisons() {


  var x, i;
  /*find all elements with an "overlay" class:*/
  x = document.getElementsByClassName("img-comp-overlay");
  for (i = 0; i < x.length; i++) {
    /*once for each "overlay" element:
    pass the "overlay" element as a parameter when executing the compareImages function:*/
    var img = x[i];
    compareImages(img);
  }

  var slider_images = document.getElementsByClassName("img-comp-img");
  for (i = 0; i < slider_images.length; i++) {
    slider_images[i].addEventListener("click", displayZoomSlider );
  }

  var fsimg = document.getElementsByClassName("fs-img-comp-overlay");
  compareFSImages(fsimg[0]);

  

  function compareImages(img) {
    var slider, img, clicked = 0, w;
    /*get the width and height of the img element*/
    w = img.offsetWidth; 
    /*set the width of the img element to 50%:*/
    img.style.width = (w / 2) + "px";
    /*create slider:*/
    slider = document.createElement("DIV");
    slider.setAttribute("class", "img-comp-slider");
    /*insert slider*/
    img.parentElement.insertBefore(slider, img);
    /*position the slider in the middle:*/
    slider.style.top = - (slider.offsetHeight / 2) + "px";
    slider.style.left = (w / 2) - (slider.offsetWidth / 2) + "px";
    setInterval( updatesize, 250 );
    function updatesize() {
      var w, pos, newwidth = document.getElementById('single_header').offsetWidth + "px";
      img.parentElement.parentElement.style.setProperty('--viewwidth',newwidth);
      pos = parseInt(slider.style.left,10) + (slider.offsetWidth / 2);
      maxpos = parseInt(newwidth,10);
      if (pos > maxpos) pos = maxpos;
      slide(pos);
    }
    /*execute a function when the mouse button is pressed:*/
    slider.addEventListener("mousedown", slideReady);
    /*and another function when the mouse button is released:*/
    window.addEventListener("mouseup", slideFinish);
    /*or touched (for touch screens:*/
    slider.addEventListener("touchstart", slideReady);
    /*and released (for touch screens:*/
    window.addEventListener("touchend", slideFinish);
    function slideReady(e) {
      /*prevent any other actions that may occur when moving over the image:*/
      e.preventDefault();
      /*the slider is now clicked and ready to move:*/
      clicked = 1;
      /*execute a function when the slider is moved:*/
      window.addEventListener("mousemove", slideMove);
      window.addEventListener("touchmove", slideMove);
    }
    function slideFinish() {
      /*the slider is no longer clicked:*/
      clicked = 0;
    }
    function slideMove(e) {
      var pos, w;
      w = img.parentElement.offsetWidth; 
      /*if the slider is no longer clicked, exit this function:*/
      if (clicked == 0) return false;
      /*get the cursor's x position:*/
      pos = getCursorPos(e)
      /*prevent the slider from being positioned outside the image:*/
      if (pos < 0) pos = 0;
      if (pos > w) pos = w;
      /*execute a function that will resize the overlay image according to the cursor:*/
      slide(pos);
    }
    function getCursorPos(e) {
      var a, x = 0;
      e = (e.changedTouches) ? e.changedTouches[0] : e;
      /*get the x positions of the image:*/
      a = img.getBoundingClientRect();
      /*calculate the cursor's x coordinate, relative to the image:*/
      x = e.pageX - a.left;
      /*consider any page scrolling:*/
      x = x - window.pageXOffset;
      return x;
    }
    function slide(x) {
      /*resize the image:*/
      img.style.width = x + "px";
      /*position the slider:*/
      slider.style.left = img.offsetWidth - (slider.offsetWidth / 2) + "px";
    }

    function playAnimation(t) {
      slide( 0.5*w+w*(0.5*Math.cos( 3.14*t/2000))*(1000/(1000+0.25*t+0.00000005*t*t*t)) );
      if (t<5000) setTimeout(function() {playAnimation(t+10);}, 1);
    } 
    playAnimation(0)
  }


  function compareFSImages(fsimg) {
    var slider, fsimg, clicked = 0, w;
    /*get the width and height of the img element*/
    w = fsimg.offsetWidth; 
    /*set the width of the img element to 50%:*/
    fsimg.style.width = (w / 2) + "px";
    /*create slider:*/
    slider = document.createElement("DIV");
    slider.setAttribute("class", "img-comp-slider-fs");
    /*insert slider*/
    fsimg.parentElement.insertBefore(slider, fsimg);
    /*position the slider in the middle:*/
    slider.style.top = - (slider.offsetHeight / 2) + "px";
    slider.style.left = (w / 2) - (slider.offsetWidth / 2) + "px";
    setInterval( updatefssize, 250 );
    function updatefssize() {
      var maxwidth = 0.8*window.innerWidth;
      var maxheight = 0.8*window.innerHeight;
      var imgwidth = document.getElementById('fs-slider-base').naturalWidth;
      var imgheight = document.getElementById('fs-slider-base').naturalHeight;
      var scale = Math.min( maxwidth/imgwidth, maxheight/imgheight )
      document.getElementById('fs-slider-base').width = scale*imgwidth;
      document.getElementById('fs-slider-overlay').width = scale*imgwidth;
      document.getElementsByClassName('fs-img-comp-container')[0].style.width = scale*imgwidth+"px";
      document.getElementsByClassName('fs-img-comp-container')[0].style.height = scale*imgheight+"px";
      
      var maxpos, pos;
      pos = parseInt(slider.style.left,10) + (slider.offsetWidth / 2);
      maxpos = parseInt(document.getElementsByClassName('fs-img-comp-container')[0].style.width);
      if (pos > maxpos) pos = maxpos;
      slide(pos);
    }
    /*execute a function when the mouse button is pressed:*/
    slider.addEventListener("mousedown", slideReady);
    /*and another function when the mouse button is released:*/
    window.addEventListener("mouseup", slideFinish);
    /*or touched (for touch screens:*/
    slider.addEventListener("touchstart", slideReady);
    /*and released (for touch screens:*/
    window.addEventListener("touchend", slideFinish);
    function slideReady(e) {
      /*prevent any other actions that may occur when moving over the image:*/
      e.preventDefault();
      /*the slider is now clicked and ready to move:*/
      clicked = 1;
      /*execute a function when the slider is moved:*/
      window.addEventListener("mousemove", slideMove);
      window.addEventListener("touchmove", slideMove);
    }
    function slideFinish() {
      /*the slider is no longer clicked:*/
      clicked = 0;
    }
    function slideMove(e) {
      var pos, w;
      w = parseInt(document.getElementsByClassName('fs-img-comp-container')[0].style.width);
      /*if the slider is no longer clicked, exit this function:*/
      if (clicked == 0) return false;
      /*get the cursor's x position:*/
      pos = getCursorPos(e)
      /*prevent the slider from being positioned outside the image:*/
      if (pos < 0) pos = 0;
      if (pos > w) pos = w;
      /*execute a function that will resize the overlay image according to the cursor:*/
      slide(pos);
    }
    function getCursorPos(e) {
      var a, x = 0;
      e = (e.changedTouches) ? e.changedTouches[0] : e;
      /*get the x positions of the image:*/
      a = fsimg.getBoundingClientRect();
      /*calculate the cursor's x coordinate, relative to the image:*/
      x = e.pageX - a.left;
      /*consider any page scrolling:*/
      x = x - window.pageXOffset;
      return x;
    }
    function slide(x) {
      /*resize the image:*/
      fsimg.style.width = x + "px";
      /*position the slider:*/
      slider.style.left = fsimg.offsetWidth - (slider.offsetWidth / 2) + "px";
    }
    // Full screen material
    var wrapper = document.getElementById("sliderzoom-wrapper");
    wrapper.addEventListener("mousedown", setClicked);
    wrapper.addEventListener("mouseup", releaseClicked);
  }

  var slidezoomVisible = false;
  
  function displayZoomSlider(e) {
    var img = e.currentTarget;
    var wrapper = document.getElementById("sliderzoom-wrapper");
    document.body.style.overflow = "hidden";
    wrapper.style.visibility = "visible";
    slidezoomVisible = true;

    document.getElementById("fs-slider-overlay").src = getComputedStyle(img.parentNode).getPropertyValue('--img0');
    document.getElementById("fs-slider-base").src = getComputedStyle(img.parentNode).getPropertyValue('--img1');
  }

  document.addEventListener("keydown", function (event) {
  // Esc to close search wrapper
  if (event.key == "Escape") {
    hideZoomSlider();
  }})

  function setClicked(){
    var wrapper = document.getElementById("sliderzoom-wrapper");
    wrapper.style.setProperty('--clicked',true);
    setTimeout(function(){wrapper.style.setProperty('--clicked',false);},200);
  }
  function releaseClicked(){
    var wrapper = document.getElementById("sliderzoom-wrapper");
    if (getComputedStyle(wrapper).getPropertyValue('--clicked') == "true"){
      hideZoomSlider();
    }
  }
  function hideZoomSlider() {
    if (slidezoomVisible) {
      var wrapper = document.getElementById("sliderzoom-wrapper");
      document.body.style.overflow = "visible";
      wrapper.style.visibility = "hidden";
      document.activeElement.blur();
      slidezoomVisible = false;
    }
  }
}
