---
title: Permissions
showDate : false
showDateUpdated : false
showLikes : false
showViews : false
showEdit : false
---

The (limitations to the) rights to reuse images from the 1982 book used to be described on the Parabolic press website, which is no longer active. It used to say *"the images may be scanned and used in lectures, face-to-face instruction, and conference presentations. Inclusion of the images in publications is not allowed under Fair Use, and permission must be requested from the original photographers for those uses"*, referring to these [Fair Use](https://fairuse.stanford.edu/overview/academic-and-educational-permissions/proposed-fair-use-guidelines/) guidelines. 

This website is inteded as an educational tool. As such, I believe that I am adhering to the fair use guidelines. Please contact [me](http://stein.stoter.gitlab.io/home) for any comments in this regard. 

Naturally, the permissions decribed by Parabolic press carry over to the images that may be found on this website.