---
title: "Contributors"

showReadingTime : false
showLikes : false
showViews : false
---

Just like the original book, this "album of computational fluid motion" is made possible by the contributions of many CFD analysis colleagues. Below follows an overview. Would you like to contribute too? Have a look at the [contribute]({{< ref "/contribute" >}}) page.
