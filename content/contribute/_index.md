---
title: "Contribute"
date: 2022-06-13T20:55:37+01:00
draft: false
layout: "simple"
showDate : false
showDateUpdated : false
showHeadingAnchors : false
showPagination : false
showReadingTime : false
showTableOfContents : false
showLikes : false
showViews : false
showEdit : false
showTaxonomies : false 
showWordCount : false
showSummary : false
sharingLinks : false
showAuthor: false
---

The progress made on this site relies largely on visitor contributions. Would you like to contribute? Here you find a how-to.

Basic procedure
---------------
As this site is automatically generated from the *Pages* Continuous Integration (CI) system of its gitlab repository, contributions to this site can be made directly through pull requests. The procedure is as follows:
1. Create a gitlab account.
2. Fork the [repository](https://www.gitlab.com/stein.stoter/album) of this website.
3. Make your changes (see details below).
4. Create a pull-request. 

The last step requests a merge of your edited version of the website into the primary repository. Once your pull-request is accepted at the end of the primary repository, the changes automatically appear on the website.



Adding a CFD analysis
---------------------
You can freely edit your fork (personal version) of the website. Additions to the different [chapters]({{< ref "chapters" >}}) are made by creating a new folder in the corresponding *"content/chapters/..."* directory. This folder should be named *"Fig..."*, with *...* the figure number corresponding to the number in the [original book]({{< ref "book" >}}). In this new folder, you add a `.jpg` screenshot of the figure from the book (**not exceeding 80kb**) called `Featured.jpg`. This automatically becomes the background of the new page, and the thumbnail of the new contribution in the chapters page. Additionally, you add an `index.md` markdown file, with a description of your CFD analysis. Have a look at the other examples to make use of all offered functionality.


Adding an authors tag
---------------------
We're all about giving credit. Adding your name to your contribution requires three things:
- A tag to your name in the *"authors"* variable in the `index.md` file of your contribution.
- A folder with the name of the chosen tag in the *"authors"* directory. In this folder you copy over an `_index.md` from any other author, and change the *"title"* variable to your name.
- A `.json` file with the name of the chosen tag in the *"data/authors"* directory. You can copy over any other author's file and change the information.

If you also add a `.jpg` image to your folder, this automatically becomes your thumbnail. Please ensure such an image is scaled down to a size of **no more than 50kb**.


Running a local version
-----------------------
While editing your work, you may want to run a local version of the website to ensure that everything looks good. To do so, you need to install [Hugo](https://gohugo.io/), a static site generator. Be sure to install the latest version (i.e., at least v0.110.0). Then, after having navigated to the repository directory, you run the following command in the command-line interface:

    hugo server

after which you can open `localhost:1313/album` in your browser to view the website.