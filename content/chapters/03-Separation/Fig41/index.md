---
title: "Fig 41. Circular cylinder at R=13.1"
date: 2023-08-18
weight: 41
featured: false
tags: ["StarCCM", "FVM", "Laminar", "Flow past cylinder", "Separation"]
authors:
  - "jobsomhorst"
---

{{< katex >}}

{{< slider "Featured.jpg" "41.jpg" "Experiment" "Simulation">}}
*"The standing eddies become elongated in the flow direction as the speed increases. Their length is found to increase linearly with Reynolds number until the flow becomes unstable above R=40."* Photograph by Sadathoshi Taneda

## Flow separation
This post is part of a series on flow separation, studied for the case of flow past a circular cylinder at different Reynolds numbers. In the current figure, it is clearly visible that the vortices behind the cylinder, which appear due to separation, grew in comparison to Figure 40. Also, one can see that the flow separates at an earlier point on the cylinder. 

The main theory and simulation and visualization set-up are discussed in the [web post from Figure 42]({{< ref "/chapters/03-Separation/Fig42" >}}). The full series is:
- [Figure 24]({{< ref "/chapters/02-Laminar/Fig24" >}}): Circular cylinder at R=1.54.
- [Figure 40]({{< ref "/chapters/03-Separation/Fig40" >}}): Circular cylinder at R=9.6.
- [Figure 41]({{< ref "/chapters/03-Separation/Fig41" >}}): Circular cylinder at R=13.1.
- [Figure 42]({{< ref "/chapters/03-Separation/Fig42" >}}): Circular cylinder at R=26.
- [Figure 45]({{< ref "/chapters/03-Separation/Fig45" >}}): Circular cylinder at R=28.4.
- [Figure 46]({{< ref "/chapters/03-Separation/Fig46" >}}): Circular cylinder at R=41.
- [Figure 96]({{< ref "/chapters/04-Vortices/Fig96" >}}): Kármán vortex street behind a circular cylinder at R=105.
- [Figure 94]({{< ref "/chapters/04-Vortices/Fig94" >}}): Kármán vortex street behind a circular cylinder at R=140.


An overview of these posts can be viewed here:

{{< youtube lGce673o8mA >}}