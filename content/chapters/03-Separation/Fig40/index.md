---
title: "Fig 40. Circular cylinder at R=9.6"
date: 2023-08-18
weight: 40
featured: false
tags: ["StarCCM", "FVM", "Laminar", "Flow past cylinder", "Separation"]
authors:
  - "jobsomhorst"
---

{{< katex >}}

{{< slider "Featured.jpg" "40.png" "Experiment" "Simulation">}}
*"Here, in contrast to figure 24, the flow has clearly separated to form a pair of recirculating eddies. The cylinder is moving through a tank of water containing aluminum powder, and is illuminated by a sheet of light below the tree surface. Extrapolation of such experiments to unbounded flow suggests separation at R=4 or 5, whereas most numerical computations give R=5 to 7."* Photograph by Sadathoshi Taneda

## Flow separation
This post is part of a series on flow separation, studied for the case of flow past a circular cylinder at different Reynolds numbers. The current figure is the first figure of this series displaying flow seperation, although the onset of separation should occur for a Reynolds number around 5.

The main theory and simulation and visualization set-up are discussed in the [web post from Figure 42]({{< ref "/chapters/03-Separation/Fig42" >}}). The full series is:
- [Figure 24]({{< ref "/chapters/02-Laminar/Fig24" >}}): Circular cylinder at R=1.54.
- [Figure 40]({{< ref "/chapters/03-Separation/Fig40" >}}): Circular cylinder at R=9.6.
- [Figure 41]({{< ref "/chapters/03-Separation/Fig41" >}}): Circular cylinder at R=13.1.
- [Figure 42]({{< ref "/chapters/03-Separation/Fig42" >}}): Circular cylinder at R=26.
- [Figure 45]({{< ref "/chapters/03-Separation/Fig45" >}}): Circular cylinder at R=28.4.
- [Figure 46]({{< ref "/chapters/03-Separation/Fig46" >}}): Circular cylinder at R=41.
- [Figure 96]({{< ref "/chapters/04-Vortices/Fig96" >}}): Kármán vortex street behind a circular cylinder at R=105.
- [Figure 94]({{< ref "/chapters/04-Vortices/Fig94" >}}): Kármán vortex street behind a circular cylinder at R=140.

An overview of these posts can be viewed here:

{{< youtube lGce673o8mA >}}