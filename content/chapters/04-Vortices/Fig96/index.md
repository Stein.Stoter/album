---
title: "Fig 96. Kármán vortex street behind a circular cylinder at R=105"
date: 2023-08-18
weight: 96
featured: false
tags: ["StarCCM", "FVM", "Laminar", "Flow past cylinder", "Vortex shedding"]
authors:
  - "jobsomhorst"
---

{{< katex >}}

{{< slider "Featured.png" "new96.png" "Experiment" "Simulation">}}
*"The initially spreading wake shown opposite develops into the two parallel rows of staggered vortices that von Kármàn's inviscid theory shows to be stable when the ratio of width to streamwise spacing is 0.28. Streaklines are shown by electrolytic precipitation in water."* Photograph by Sadathoshi Taneda

## Flow separation
This post is part of a series on flow separation, studied for the case of flow past a circular cylinder at different Reynolds numbers. In the current figure, one finds a longer version of the von Kármán street as that shown in [Figure 94]({{< ref "/chapters/04-Vortices/Fig94" >}}). In comparison to Figure 94, the photograph of this street is taken at a later time step. The vortices in the current figure all appear at the same height compared to the cylinder, whereas the vortices in Figure 94 succesively increase in height; an artifact of the start-up  of the vortex shedding. When a later time step of the flow in Figure 94 were to be taken, the vortices also even out and appear at the same height.

The main theory on flow separation and visualization set-up are discussed in the [web post from Figure 42]({{< ref "/chapters/03-Separation/Fig42" >}}). More information on the von Kármán vortex street can be found in the [web post from Figure 94]({{< ref "/chapters/04-Vortices/Fig94" >}}). The full series is:
- [Figure 24]({{< ref "/chapters/02-Laminar/Fig24" >}}): Circular cylinder at R=1.54.
- [Figure 40]({{< ref "/chapters/03-Separation/Fig40" >}}): Circular cylinder at R=9.6.
- [Figure 41]({{< ref "/chapters/03-Separation/Fig41" >}}): Circular cylinder at R=13.1.
- [Figure 42]({{< ref "/chapters/03-Separation/Fig42" >}}): Circular cylinder at R=26.
- [Figure 45]({{< ref "/chapters/03-Separation/Fig45" >}}): Circular cylinder at R=28.4.
- [Figure 46]({{< ref "/chapters/03-Separation/Fig46" >}}): Circular cylinder at R=41.
- [Figure 96]({{< ref "/chapters/04-Vortices/Fig96" >}}): Kármán vortex street behind a circular cylinder at R=105.
- [Figure 94]({{< ref "/chapters/04-Vortices/Fig94" >}}): Kármán vortex street behind a circular cylinder at R=140.

An overview of these posts can be viewed here:

{{< youtube lGce673o8mA >}}

The computational domain used to simulate the flow for the current figure differs in length and height from that of Figure 94, and is illustrated in the Figure below.

{{< figure     
    src="domain.png"
    caption="Computational domain, boundary conditions and mesh."
    alt="fig96 domain"
    >}}
